# H-Bridge Motor Controller

A MOSFET powered, PWM-controlled DC motor driver.

Capable of driving 12V DC motors with up 20A of current.

## Features

* Power MOSFET H-bridge driver
* Flyback diodes
* Forward, backward, free-wheeling and braking modes supported
* Learning mode to tune the input PWM signal range
* Wide input voltage range: 6V-18V
* Up to 20A of drive current
* Voltage and current sensors to prevent damage
* Status indicator LEDs
* 3-pin PWM header, compatible with common RC receivers

## Assembly

The 4 soldering holes on the top left of the board (marked as J1) are intended
for a 4-pin terminal block. It's also possible to solder wires directly into
them, if desired.

The LEDs should have different colors to differentiate their function.
From top to bottom:
* D1 Power (green)
* D2 Ready (green)
* D5 Fault (red)
* D6 Learn idle duty cycle (yellow or other color)
* D7 Learn forward duty cycle (yellow or other color)
* D8 Learn backward duty cycle (yellow or other color)

R15 should not be soldered, unless you intend to power the microcontroller
from the RC receiver power supply. In this case, do not solder C1 and U2.

Push button SW2 triggers a hardware reset. Pressing push button SW1 during
power-on (or reset) enables learning mode.

## Firmware

The firmware source code is contained in the firmware folder. You need GNU make,
avr-gcc and avr-libc to build it, and avrdude to flash it.

The makefile is prepared for use with a USBtiny programmer. Change as needed.

To compile and flash the firmware, connect the programmer's 6-pin ISP header to
J3 (pin one is marked on the PCB), then run the following commands:

```shell
cd firmware/
make
sudo make flash
```

## Copyright

Hardware schematics and other documentation are Copyright © 2021 Gregor Riepl

Licensed under the CERN Open Hardware License v1.2. See the file
LICENSE.hardware for details.

Firmware source code and compilation scripts are Copyright © 2021 Gregor Riepl

Licensed under the GNU General Public License Version 3. See the file
LICENSE.firmware for details.

/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>

// Time base: 64/F_CPU in microseconds
#define TIMING_BASE_CLOCK ((uint16_t) (1000000UL * 64 / F_CPU))

// Initializes the PWM control input measurement.
// Uses timer 1.
void timing_init();

// Reads the current timing value.
uint16_t timing_read();

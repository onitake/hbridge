/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "timing.h"

static uint8_t timing_pinb0 = 0;
static uint16_t timing_rising = 0;
static uint16_t timing_value = 0;

ISR(PCINT0_vect) {
	// cache the pin input state
	uint8_t pinb0 = PINB & _BV(PB0);
	// did the input change?
	if (pinb0 != timing_pinb0) {
		// check if we got a rising or falling edge
		if (pinb0) {
			timing_rising = TCNT1;
		} else {
			// for unsigned values, this should work both ways
			// if timing_rising > timing_falling, subtraction will simply wrap around 0
			timing_value = TCNT1 - timing_rising;
		}
	}
	// update the cached pin state
	timing_pinb0 = pinb0;
}

void timing_init() {
	// PB0 (ICP1): input, pullup on
	DDRB &= ~(_BV(PB0));
	PORTB |= _BV(PB0);

	// initialize timer 1
	// f = clkio/64 = 250kHz
	// min = 1/f = 4us
	// max = 65535/f = ~262ms
	TCCR1A = 0;
	TCCR1B = _BV(CS11) | _BV(CS10);
	TCCR1C = 0;

	// ICP can only capture one edge, so we'd have to flip the direction constantly :(
	// let's use a GPIO interrupt instead and do it in software
	PCMSK0 |= _BV(PCINT0);
	PCICR |= _BV(PCIE0);
}

uint16_t timing_read() {
	return timing_value;
}

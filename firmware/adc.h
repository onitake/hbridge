/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

void adc_init();

// current sensor: 2.5V=0A 0.5V=-50A 4.5V=50A
// current = (ADC * 5.0V / 0x3ff - 2.5V) * 50A / 2V
#define ADC_CURRENT_REAL(adc) ((adc * 5.0 / 0x3ff - 2.5) * 50.0 / 2.0)
#define ADC_CURRENT_VALUE(ampere) ((uint16_t) ((ampere * 2.0 / 50.0 + 2.5) * 0x3ff / 5.0))
uint16_t adc_current();

// voltage sensor: 0V=0V 5V=5V*(3.3k+6.8k)/3.3k=15.3V
// voltage = (ADC * 5.0V / 0x3ff) * (3.3 + 6.8) / 3.3
#define ADC_VOLTAGE_REAL(adc) (adc * 5.0 / 0x3ff * (3.3 + 6.8) / 3.3)
#define ADC_VOLTAGE_VALUE(volt) ((uint16_t) (volt * 3.3 / (3.3 + 6.8) * 0x3ff / 5.0))
uint16_t adc_voltage();

// temperature sensor: arbitrary result, needs to be calibrated per device
// voltage = (ADC * 1.1V / 0x3ff)
// data sheet: 242mV=-45° 314mV=25° 380mV=85°
// measured on one device: 392mV=20° 396mV=35°
#define ADC_TEMP_REAL(adc) (volt * 1.1 / 0x3ff)
#define ADC_TEMP_VALUE(volt) ((uint16_t) (volt / 1.1 * 0x3ff))
uint16_t adc_temperature();

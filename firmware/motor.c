/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "motor.h"

#ifndef MOTOR_MIN_THRESHOLD
#define MOTOR_MIN_THRESHOLD 1
#endif
#ifndef MOTOR_MAX_THRESHOLD
#define MOTOR_MAX_THRESHOLD 254
#endif

// Motor direction
static bool motor_forward = true;

// PWM generator
ISR(TIMER0_OVF_vect) {
	// reset outputs on overflow
	// all drivers low
	PORTC &= ~(_BV(PC0) | _BV(PC1));
	//PORTD &= ~(_BV(PD5) | _BV(PD6));
	// off stage: PD5 on, PD6 on, PC0 off, PC1 off
	PORTD |= _BV(PD5) | _BV(PD6);
}

ISR(TIMER0_COMPA_vect) {
	// set outputs on compare match
	// all drivers low
	PORTC &= ~(_BV(PC0) | _BV(PC1));
	PORTD &= ~(_BV(PD5) | _BV(PD6));
	// on stage: PD5 on, PC0 on, PD6 off, PC1 off
	PORTC |= _BV(PC0);
	PORTD |= _BV(PD5);
}

ISR(TIMER0_COMPB_vect) {
	// set outputs on compare match
	// all drivers low
	PORTC &= ~(_BV(PC0) | _BV(PC1));
	PORTD &= ~(_BV(PD5) | _BV(PD6));
	// on stage: PD6 on, PC1 on, PD5 off, PC0 off
	PORTC |= _BV(PC1);
	PORTD |= _BV(PD6);
}

void motor_init() {
	// PC0: forward high-side, A1
	// PC1: backward high-side, A2
	// PD5, OC0B: forward low-side, PWM, B2
	// PD6, OC0A: backward low-side, PWM, B1

	// CAUTION Never switch both PC0+PD6 or PC1+PD5 on at the same time!
	// This will short-circuit the MOSFETs and destroy them.
	// The current sensor is placed between one half-bridge and the output,
	// it won't detect short-circuits across the half-bridges.
	// Note that the MOSFET driver inverts the high-side signal, so all four
	// outputs are active-high on the microcontroller.
	// NOTE The gate drivers and MOSFETs have a limited switching speed.

	// set all outputs to low
	PORTC &= ~(_BV(PC0) | _BV(PC1));
	PORTD &= ~(_BV(PD5) | _BV(PD6));
	// enable GPIO outputs
	// high-side
	DDRC |= _BV(PC0) | _BV(PC1);
	// low-side
	DDRD |= _BV(PD5) | _BV(PD6);

	// enable and configure timer0
	// reset compare values
	OCR0A = 0;
	OCR0B = 0;
	// fast pwm, update at 0, max 255
	TCCR0A = _BV(WGM00) | _BV(WGM01);
	// 16mhz/255/8 = 7.84khz
	TCCR0B = _BV(CS01);
	// enable overflow interrupt to ensure outputs are off
	TIMSK0 = _BV(TOIE0);
}

void motor_direction(bool forward) {
	motor_forward = forward;
	motor_power(0);
}

void motor_power(uint8_t power) {
	if (motor_forward) {
		OCR0A = 255 - power;
		if (power < MOTOR_MIN_THRESHOLD) {
			// off, enable only overflow
			TIMSK0 = _BV(TOIE0);
		} else if (power > MOTOR_MAX_THRESHOLD) {
			// full, enable only compare
			TIMSK0 = _BV(OCIE0A);
		} else {
			// intermediate, enable both
			TIMSK0 = _BV(OCIE0A) | _BV(TOIE0);
		}
	} else {
		OCR0B = 255 - power;
		if (power < MOTOR_MIN_THRESHOLD) {
			// off, enable only overflow
			TIMSK0 = _BV(TOIE0);
		} else if (power > MOTOR_MAX_THRESHOLD) {
			// full, enable only compare
			TIMSK0 = _BV(OCIE0B);
		} else {
			// intermediate, enable both
			TIMSK0 = _BV(OCIE0B) | _BV(TOIE0);
		}
	}
}

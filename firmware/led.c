/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include "led.h"

void led_init() {
	// PD0, PD1, PD2, PD3, PD4: output, low
	// PD0 LRNBWD
	// PD1 LRNFWD
	// PD2 LRNIDLE
	// PD3 FAULT
	// PD4 READY
	PORTD &= ~(_BV(PD0) | _BV(PD1) | _BV(PD2) | _BV(PD3) | _BV(PD4));
	DDRD |= _BV(PD0) | _BV(PD1) | _BV(PD2) | _BV(PD3) | _BV(PD4);
}

void led_pattern(uint8_t pattern) {
	PORTD = (PORTD & ~(_BV(PD0) | _BV(PD1) | _BV(PD2) | _BV(PD3) | _BV(PD4))) | (pattern & 0x1f);
}

void led_off() {
	PORTD = (PORTD & ~(_BV(PD0) | _BV(PD1) | _BV(PD2) | _BV(PD3) | _BV(PD4)));
}

void led_ready() {
	led_pattern(_BV(PD4));
}

void led_fault() {
	led_pattern(_BV(PD3));
}

void led_forward() {
	led_pattern(_BV(PD1));
}

void led_backward() {
	led_pattern(_BV(PD0));
}

void led_idle() {
	led_pattern(_BV(PD2));
}

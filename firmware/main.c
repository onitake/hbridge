/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "adc.h"
#include "led.h"
#include "motor.h"
#include "timing.h"

#if 0

#define STATE_INIT 0
#define STATE_FAULT 1
#define STATE_LEARN 2
#define STATE_SELFTEST 3
#define STATE_READY 4
#define STATE_FREEZE 5
static uint8_t state = STATE_INIT;

uint8_t state_init();
uint8_t state_learn();
uint8_t state_selftest();
uint8_t state_ready();
uint8_t state_fault();

/* I/O map:
	PB0 PWM
	PB3 MOSI
	PB4 MISO
	PB5 SCK
	PC0 A1
	PC1 A2
	PC2 ISENS
	PC3 VSENS
	PC6 RESET
	PD0 LRNBWD
	PD1 LRNFWD
	PD2 LRNIDLE
	PD3 FAULT
	PD4 READY
	PD5 B2
	PD6 B1
	PD7 LEARN
*/

uint8_t state_init() {
	// initialize the buttons
	// PD7: input, no pullup
	PORTD &= ~_BV(PD7);
	DDRD &= ~_BV(PD7);

	// initialize the LEDs
	led_init();

	// initialize the ADC
	adc_init();

	// initialize the motor controller
	motor_init();

	// initialize the PWM detector
	timing_init();

	// enable interrupts
	//sei();

	// enter selftest state
	//return STATE_SELFTEST;
	return STATE_READY;
}

uint8_t state_learn() {
	// decide how to continue
	bool learning = false;
	if (PIND & _BV(PD7)) {
		// learn button pressed
		learning = true;
	}
	if (false) {
		// TODO enter learning state if EEPROM is empty
		learning = true;
	}

	if (learning) {
		led_idle();
		led_forward();
		led_backward();
		// TODO
		led_off();
	}

	return STATE_READY;
}

static uint16_t pwm_min = -1;
static uint16_t pwm_max = 0;

uint8_t state_selftest() {
	//led_off();

	//uint16_t value = adc_temperature();
	//uint16_t value = adc_current();
	//uint16_t value = adc_voltage();
	//led_pattern(value >> 5);
	//_delay_ms(500);
	//led_pattern(value);
	//_delay_ms(500);
	//led_off();
	//_delay_ms(500);

	// wait until we have valid timing information
	/*timing_clear();
	while (!timing_update());
	uint16_t active = timing_active();
	led_pattern((active >> 12) | 0x10);
	_delay_ms(500);
	led_pattern((active >> 8) | 0x10);
	_delay_ms(500);
	led_pattern((active >> 4) | 0x10);
	_delay_ms(500);
	led_pattern((active >> 0) | 0x10);
	_delay_ms(500);
	led_off();
	_delay_ms(2000);*/

	/*timing_clear();
	while (!timing_update());
	uint16_t active = timing_active();
	if (active < pwm_min) pwm_min = active;
	if (active > pwm_max) pwm_max = active;
	uint16_t pwm_mid = pwm_min + (pwm_max - pwm_min) / 2;

	if (active < pwm_mid - 20) {
		led_backward();
		motor_direction(MOTOR_BACKWARD);
		uint16_t diff = pwm_mid - active;
		uint16_t scaled = diff * 255 / ((pwm_max - pwm_min) / 2);
		if (scaled <= 255) {
			motor_power((uint8_t) scaled);
		} else {
			motor_power(255);
		}
	} else if (active > pwm_mid + 20) {
		led_forward();
		motor_direction(MOTOR_FORWARD);
		uint16_t diff = active - pwm_mid;
		uint16_t scaled = diff * 255 / ((pwm_max - pwm_min) / 2);
		if (scaled <= 255) {
			motor_power((uint8_t) scaled);
		} else {
			motor_power(255);
		}
	} else {
		led_idle();
		motor_direction(MOTOR_IDLE);
	}*/

	//uint8_t level = active - pwm_min / (pwm_max - pwm_min);
	/*motor_direction(MOTOR_FORWARD);
	motor_power(31);
	_delay_ms(5000);
	uint16_t value = adc_current();
	motor_direction(MOTOR_IDLE);
	led_pattern(value >> 5);
	_delay_ms(500);
	led_pattern(value);
	_delay_ms(500);
	led_off();
	_delay_ms(500);
	motor_direction(MOTOR_BACKWARD);
	motor_power(15);
	_delay_ms(5000);
	value = adc_current();
	motor_direction(MOTOR_IDLE);
	led_pattern(value >> 5);
	_delay_ms(500);
	led_pattern(value);
	_delay_ms(500);
	led_off();
	_delay_ms(500);*/

	// are we connected to external power, and is there enough voltage
	// for stable operation?
	if (adc_voltage() < ADC_VOLTAGE_VALUE(4.7)) {
		return STATE_FAULT;
	}
	// done, decide if learning is needed
	//return STATE_LEARN;

	return STATE_READY;
}

uint8_t state_ready() {
#define STEP_DELAY 100
#define POWER_MAX 31
	// motor test
	motor_direction(MOTOR_FORWARD);
	led_forward();
	// ramp-up
	for (uint8_t i = 0; i < POWER_MAX; i++) {
		motor_power(i);
		_delay_ms(STEP_DELAY);
	}
	// ramp-down
	for (uint8_t i = 0; i < POWER_MAX; i++) {
		motor_power(POWER_MAX - i);
		_delay_ms(STEP_DELAY);
	}
	motor_direction(MOTOR_BACKWARD);
	led_backward();
	// ramp-up
	for (uint8_t i = 0; i < POWER_MAX; i++) {
		motor_power(i);
		_delay_ms(STEP_DELAY);
	}
	// ramp-down
	for (uint8_t i = 0; i < POWER_MAX; i++) {
		motor_power(POWER_MAX - i);
		_delay_ms(STEP_DELAY);
	}
	motor_direction(MOTOR_IDLE);
	led_idle();
	_delay_ms(POWER_MAX*STEP_DELAY);
	return STATE_READY;
}

uint8_t state_fault() {
	// display fault state
	led_fault();
	// return to idle
	motor_direction(MOTOR_IDLE);
	return STATE_FREEZE;
}

int main2() {
	while (1) {
		switch (state) {
			case STATE_INIT:
				state = state_init();
				break;
			case STATE_FAULT:
				state = state_fault();
				break;
			case STATE_LEARN:
				state = state_learn();
				break;
			case STATE_SELFTEST:
				state = state_selftest();
				break;
			case STATE_READY:
				state = state_ready();
				break;
			case STATE_FREEZE:
				// we could also return from main here, or cli and enter sleep mode
				break;
		}
	}
}

#endif

// Pulse measurements in timing base units
// Center dead zone: 50us / 4us = 12
#define PULSE_CENTER_DEAD ((uint16_t) (50 / TIMING_BASE_CLOCK))
// Minimum limit: 900us / 4us = 225
#define PULSE_MIN_LIMIT ((uint16_t) (900 / TIMING_BASE_CLOCK))
// Maximum limit: 2100us / 4us = 525
#define PULSE_MAX_LIMIT ((uint16_t) (2100 / TIMING_BASE_CLOCK))
// Minimum clamp: 1100us / 4us = 275
#define PULSE_MIN ((uint16_t) (1150 / TIMING_BASE_CLOCK))
// Center: 1500us / 4us = 375
#define PULSE_CENTER ((uint16_t) (1500 / TIMING_BASE_CLOCK))
// Maximum clamp: 1900us / 4us = 475
#define PULSE_MAX ((uint16_t) (1850 / TIMING_BASE_CLOCK))

int main() {
	// RC pulse range:
	// vertical: down: down trim: 0.995ms center trim: 1.105ms up trim: 1.215ms up: down trim: 1.795ms center trim: 1.905ms up trim: 2.015ms
	// horizontal: left: left trim: 2.045ms center trim: 1.925ms right trim: 1.815ms right: left trim: 1.215ms center trim: 1.095ms right trim: 0.955ms
	// range: 0.95ms..1.50ms..2.05ms
	// center dead zone: +/-0.05ms
	// min/max dead zone: 0.1ms

	// initialize hardware
	motor_init();
	timing_init();

	// enable interrupts
	sei();

	// sweep state
	bool forward = true;
	uint8_t gauge = 0;
	bool up = true;
	
	while (1) {
#if 0
		// update mode
		if (up) {
			if (gauge == 255) {
				up = false;
				_delay_ms(3000);
			} else {
				gauge++;
				motor_power(gauge);
			}
		} else {
			if (gauge == 0) {
				forward = !forward;
				motor_direction(forward);
				up = true;
				_delay_ms(3000);
			} else {
				gauge--;
				motor_power(gauge);
			}
		}
#else
		uint16_t pulse = timing_read();
		if (pulse < PULSE_MIN_LIMIT) {
			// pulse too short, ignore
		} else if (pulse > PULSE_MAX_LIMIT) {
			// pulse too long, ignore
		} else if (pulse < PULSE_CENTER - PULSE_CENTER_DEAD) {
			// backward, set throttle
			if (pulse <= PULSE_MIN) {
				gauge = 255;
			} else {
				gauge = 255 - (uint8_t) ((pulse - PULSE_MIN) * 255 / (PULSE_CENTER - PULSE_MIN));
			}
			if (forward) {
				forward = false;
				motor_direction(forward);
			}
			motor_power(gauge);
		} else if (pulse > PULSE_CENTER + PULSE_CENTER_DEAD) {
			// forward, set throttle
			if (pulse >= PULSE_MAX) {
				gauge = 255;
			} else {
				gauge = (uint8_t) ((pulse - PULSE_CENTER) * 255 / (PULSE_MAX - PULSE_CENTER));
			}
			if (!forward) {
				forward = true;
				motor_direction(forward);
			}
			motor_power(gauge);
		} else {
			// center, idle
			motor_power(0);
		}
#endif
		_delay_ms(50);
	}
}

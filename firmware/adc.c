/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include "adc.h"

void adc_init() {
	// initialize the ADC
	// enable, F_CPU/128 clock (=125kHz @16MHz)
	ADCSRA = _BV(ADEN) | _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);
	// start a dummy conversion to init the ADC
	adc_temperature();
}

uint16_t adc_current() {
	// wait (forever) if a conversion is in progress
	while (ADCSRA & _BV(ADSC));
	// Isense: PD2 / ADC2, AVcc (5V)
	ADMUX = _BV(REFS0) | 2;
	// start conversion
	ADCSRA |= _BV(ADSC);
	// wait (forever) for completion
	// TODO use interrupts
	while (ADCSRA & _BV(ADSC));
	return ADC;
}

uint16_t adc_voltage() {
	// wait (forever) if a conversion is in progress
	while (ADCSRA & _BV(ADSC));
	// Vsense: PD3 / ADC3, AVcc (5V)
	ADMUX = _BV(REFS0) | 3;
	// start conversion
	ADCSRA |= _BV(ADSC);
	// wait (forever) for completion
	// TODO use interrupts
	while (ADCSRA & _BV(ADSC));
	return ADC;
}

uint16_t adc_temperature() {
	// wait (forever) if a conversion is in progress
	while (ADCSRA & _BV(ADSC));
	// Temperature: ADC8, Vref (1.1V)
	ADMUX = _BV(REFS0) | _BV(REFS1) | 8;
	// start conversion
	ADCSRA |= _BV(ADSC);
	// wait (forever) for completion
	// TODO use interrupts
	while (ADCSRA & _BV(ADSC));
	return ADC;
}


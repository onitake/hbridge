/*
 * h-bridge motor controller
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>

// Initializes the GPIO outputs and puts the motor in idle state
// The motor driver uses timer 0.
void motor_init();

// Change direction of the motor:
// If the argument is true, move forward.
// If it is false, move backward.
// Resets the duty cycle to zero, so don't forget to call motor_power afterwards.
void motor_direction(bool forward);

// Sets the PWM duty cycle of the motor: MOTOR_MIN_THRESHOLD..MOTOR_MAX_THRESHOLD
// Does not change direction.
// A value below the threshold turns off.
// PWM frequency = ~7.8kHz
// Default MOTOR_MIN_THRESHOLD = 1
// Default MOTOR_MAX_THRESHOLD = 254
void motor_power(uint8_t power);

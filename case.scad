/*
 * h-bridge motor controller
 * 3D-printable PCB case
 *
 * Copyright (C) 2021, Gregor Riepl <onitake@gmail.com>
 *
 * This source describes Open Hardware and is licensed under the CERN-OHL v1.2
 *
 * You may redistribute and modify this source and make products using it under
 * the terms of the CERN-OHL-1.2 (LICENSE.hardware).
 *
 * This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY,
 * INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
 * PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.
 */

$fn=50;

epsilon=.1;

pcb_width=65;
pcb_length=45;
pcb_margin=.5;
pcb_corner=3;
pcb_thickness=1.6;

seat_height=3;
seat_corner=3;
footprint_height=10;

wall_thickness=1;
cutout_left=2;
cutout_right=18;

clamp_thickness=1.4;
clamp_height=0.35;

*difference() {
	cube([pcb_width + pcb_margin*2 + wall_thickness*2, pcb_length + pcb_margin*2 + wall_thickness*2, footprint_height + seat_height + wall_thickness]);
	translate([wall_thickness, wall_thickness, wall_thickness]) {
		difference() {
			cube([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, footprint_height + seat_height + epsilon]);
			rotate([0, 0, 45]) {
				cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
			}
			translate([-wall_thickness, -wall_thickness, seat_height + pcb_thickness]) {
				scale([1, 1, clamp_height]) {
					sphere(seat_corner + clamp_thickness);
				}
			}
			translate([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, 0]) {
				rotate([0, 0, 45]) {
					cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
				}
				translate([wall_thickness, wall_thickness, seat_height + pcb_thickness]) {
					scale([1, 1, clamp_height]) {
						sphere( + clamp_thickness);
					}
				}
			}
			translate([-epsilon, -epsilon, -epsilon]) {
				cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
			}
			translate([pcb_width + pcb_margin*2 - seat_corner + epsilon, -epsilon, -epsilon]) {
				cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
			}
			translate([pcb_width + pcb_margin*2 - seat_corner + epsilon, pcb_length + pcb_margin*2 - seat_corner + epsilon, -epsilon]) {
				cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
			}
			translate([-epsilon, pcb_length + pcb_margin*2 - seat_corner + epsilon, -epsilon]) {
				cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
			}
		}
	}
	translate([wall_thickness + cutout_left + pcb_margin, pcb_length + wall_thickness + pcb_margin*2 - epsilon, seat_height + pcb_thickness + wall_thickness]) {
		cube([pcb_width - cutout_left - cutout_right, wall_thickness + epsilon*2, footprint_height + epsilon]);
	}
}

%translate([wall_thickness, wall_thickness, wall_thickness]) {
	difference() {
		cube([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, footprint_height + seat_height + epsilon]);
		rotate([0, 0, 45]) {
			cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
		}
		translate([-wall_thickness, -wall_thickness, seat_height + pcb_thickness]) {
			scale([1, 1, clamp_height]) {
				sphere(seat_corner + clamp_thickness);
			}
		}
		translate([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, 0]) {
			rotate([0, 0, 45]) {
				cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
			}
			translate([wall_thickness, wall_thickness, seat_height + pcb_thickness]) {
				scale([1, 1, clamp_height]) {
					sphere( + clamp_thickness);
				}
			}
		}
		translate([-epsilon, -epsilon, -epsilon]) {
			cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
		}
		translate([pcb_width + pcb_margin*2 - seat_corner + epsilon, -epsilon, -epsilon]) {
			cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
		}
		translate([pcb_width + pcb_margin*2 - seat_corner + epsilon, pcb_length + pcb_margin*2 - seat_corner + epsilon, -epsilon]) {
			cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
		}
		translate([-epsilon, pcb_length + pcb_margin*2 - seat_corner + epsilon, -epsilon]) {
			cube([seat_corner + epsilon, seat_corner + epsilon, seat_height + epsilon*2 + epsilon]);
		}
	}
}

translate([wall_thickness, wall_thickness, wall_thickness]) {
	difference() {
		union() {
			translate([0, 0, footprint_height + seat_height - wall_thickness]) {
				cube([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, wall_thickness]);
			}
			translate([0, 0, seat_height + pcb_thickness]) {
				cube([seat_corner, seat_corner, footprint_height - pcb_thickness]);
			}
			translate([pcb_width + pcb_margin*2 - seat_corner, 0, seat_height + pcb_thickness]) {
				cube([seat_corner, seat_corner, footprint_height - pcb_thickness]);
			}
			translate([pcb_width + pcb_margin*2 - seat_corner, pcb_length + pcb_margin*2 - seat_corner, seat_height + pcb_thickness]) {
				cube([seat_corner, seat_corner, footprint_height - pcb_thickness]);
			}
			translate([0, pcb_length + pcb_margin*2 - seat_corner, seat_height + pcb_thickness]) {
				cube([seat_corner, seat_corner, footprint_height - pcb_thickness]);
			}
		}
		rotate([0, 0, 45]) {
			cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
		}
		translate([pcb_width + pcb_margin*2, pcb_length + pcb_margin*2, 0]) {
			rotate([0, 0, 45]) {
				cube([pcb_corner * sqrt(2), pcb_corner * sqrt(2), (footprint_height + seat_height + epsilon)*2 + epsilon], center=true);
			}
		}
	}
}